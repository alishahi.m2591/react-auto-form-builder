import React from "react";
import Header from "./component/generateTable/header";
import Footer from "./component/generateTable/footer";
import Body from "./component/generateTable/Body";


function App() {
    const columns = {
        id: 'id',
        title: "title",
        price: 'price',
        purChased_at:'purChased_at'
    };

    const data = [
        {
            id: 1,
            title: "Smart Watch",
            price: 3000,
            purChase_at:'November 2019'
        } ,
        {
            id: 2,
            title: "ماشین شاسی بلند خارجی",
            price: 350000,
            purChase_at:'فروردین 99'
        } ,
        {
            id: 3,
            title: "گوشی اپل",
            price: 18000,
            purChase_at:'آذر 98'
        } ,
    ];

    return (
    <div className="p-5">
        <table className="table table-hover table-bordered ">
            <Header columns={columns}/>
            <Body data={data}/>
            <Footer columns={columns}/>
        </table>
    </div>
    );

}

export default App;
